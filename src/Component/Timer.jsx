import { useState } from "react";

export const Timer = () => {
  const [time, setTime] = useState({
    h_tens: 0,
    h_once: 0,
    m_tens: 0,
    m_once: 0,
    s_tens: 0,
    s_once: 0,
    ms: 0,
  });
  const [interv, setInterv] = useState();
  const [status, setStatus] = useState("reset");
  const [lap, setLap] = useState([]);
  const lapFunction = () => {
    setLap([{ ...time }, ...lap]);
  };
  const Start = () => {
    if (!interv) {
      run();
      setInterv(setInterval(run, 100));
      setStatus("start");
    }
  };
  const Stop = () => {
    clearInterval(interv);
    setInterv(false);
    setStatus("stop");
  };
  const Reset = () => {
    clearInterval(interv);
    setTime({
      h_tens: 0,
      h_once: 0,
      m_tens: 0,
      m_once: 0,
      s_tens: 0,
      s_once: 0,
      ms: 0,
    });
    setLap([]);
    setStatus("reset");
  };
  let UpdatedMs = time.ms,
    UpdatedS_once = time.s_once,
    UpdatedS_tens = time.s_tens,
    UpdatedM_once = time.m_once,
    UpdatedM_tens = time.m_tens,
    UpdatedH_once = time.h_once,
    UpdatedH_tens = time.h_tens;

  const run = () => {
    if (UpdatedH_once == 9) {
      ++UpdatedH_tens;
      UpdatedH_once = 0;
    }
    if (UpdatedM_tens == 5) {
      ++UpdatedH_once;
      UpdatedM_tens = 0;
    }
    if (UpdatedM_once == 9) {
      ++UpdatedM_tens;
      UpdatedM_once = 0;
    }
    if (UpdatedS_tens == 5) {
      ++UpdatedM_once;
      UpdatedS_tens = 0;
    }
    if (UpdatedS_once == 9) {
      ++UpdatedS_tens;
      UpdatedS_once = 0;
    }
    if (UpdatedMs == 9) {
      ++UpdatedS_once;
      UpdatedMs = 0;
    }
    ++UpdatedMs;
    return setTime({
      h_tens: UpdatedH_tens,
      h_once: UpdatedH_once,
      m_tens: UpdatedM_tens,
      m_once: UpdatedM_once,
      s_tens: UpdatedS_tens,
      s_once: UpdatedS_once,
      ms: UpdatedMs,
    });
  };

  return (
    <div>
      <div className="App">
        <div className="container">
          <div className="time">{`${time.h_tens}`}</div>
          <div className="time">{`${time.h_once}`}</div>:
          <div className="time">{`${time.m_tens}`}</div>
          <div className="time">{`${time.m_once}`}</div>:
          <div className="time">{`${time.s_tens}`}</div>
          <div className="time">{`${time.s_once}`}</div>:
          <div className="time">{`${time.ms}`}</div>
        </div>
        <div className="buttonContainer">
          {status === "reset" || status === "stop" ? (
            <button
              className="button"
              onClick={() => {
                Start();
              }}
            >
              Start
            </button>
          ) : null}
          {status === "start" ? (
            <button
              className="button"
              onClick={() => {
                Stop();
              }}
            >
              Stop
            </button>
          ) : null}
          {status === "stop" ? (
            <button
              className="button"
              onClick={() => {
                Reset();
              }}
            >
              Reset
            </button>
          ) : null}
          {status === "start" ? (
            <button
              className="button"
              onClick={() => {
                lapFunction();
              }}
            >
              Lap
            </button>
          ) : null}
        </div>
      </div>
      {lap.length > 0 ? (
        <div className="LapContainer">
          Lap:
          <div className="Lap">
            {lap.map((item) => {
              return (
                <div>{`${item.h_tens}${item.h_once}:${item.m_tens}${item.m_once}:${item.s_tens}${item.s_once}:${item.ms}`}</div>
              );
            })}
          </div>
        </div>
      ) : null}
    </div>
  );
};
